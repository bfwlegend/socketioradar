#include <sio_client.h>
#include <iostream>
#include <Windows.h>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <tchar.h>
#include <thread>
#include <vector>
#include <Tlhelp32.h>
#include "Offsets.h"
#include "json/json.h"

HWND hwndWindow = NULL;
DWORD pID, clientDLL, engineDLL;
DWORD dwRadar, dwTemp;
HANDLE hanProcess = NULL;
std::string oldmap;
std::string ip;
std::stringstream ss;
Json::Value Players;
sio::client h;
int fps = 10;

DWORD GetModuleBase(const DWORD dwProcessId, const char* szModuleName);
DWORD GetEntityById( int iIndex );
void MainStuff();
void processStuff();
void getIP();
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	//TODO: Add config options
	//Load from same file as IP
	//Use config4cpp possibly
	getIP();
	Sleep(3000);
	hwndWindow = FindWindow(NULL,"Counter-Strike: Global Offensive");
	cout<<"Waiting for game...";
	while (hwndWindow == NULL)
	{
		hwndWindow = FindWindow(NULL,"Counter-Strike: Global Offensive");
		Sleep(1000);
	}
	GetWindowThreadProcessId(hwndWindow, &pID);
	hanProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, pID);
	clientDLL = GetModuleBase(pID, "client.dll");
	engineDLL = GetModuleBase(pID, "engine.dll");
	Beep (600,100);
	Beep (750,100);
	Beep (800,100);
	thread csgo(MainStuff);
	//TODO: Make detection a bit better
	Start:
	processStuff();
	while(true)
	{
		hwndWindow = FindWindow(NULL,"Counter-Strike: Global Offensive"); if (hwndWindow == NULL) { goto Start; } Sleep(5000);
	}
	return 0;
}

DWORD GetModuleBase(const DWORD dwProcessId, const char* szModuleName)
{
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);

	if(!hSnap)
	{
		return 0;
	}

	MODULEENTRY32 me;
	me.dwSize = sizeof(MODULEENTRY32);
	DWORD dwReturn = 0;

	if(Module32First(hSnap, &me))
	{
		while(Module32Next(hSnap, &me))
		{
			if(lstrcmpi(me.szModule, szModuleName) == 0)
			{
				dwReturn = (DWORD) me.modBaseAddr;
				break;
			}
		}
	}

	CloseHandle(hSnap);
	return dwReturn;
}
DWORD GetEntityById( int iIndex ){
    DWORD dwEntity;
    ReadProcessMemory( hanProcess, LPCVOID( ( clientDLL + EntityList + iIndex * 0x10 ) - 0x10 ), &dwEntity, sizeof( DWORD ), NULL );
    return dwEntity;
}
void MainStuff(){
	h.connect(ip);
	while(true)
	{
		Players.clear();
		LocalEntity.Reset();
		//Get LocalPlayer Bases
		ReadProcessMemory (hanProcess, (LPVOID) (clientDLL + LocalPlayer), &LocalEntity.Base, sizeof(LocalEntity.Base), NULL);
		ReadProcessMemory (hanProcess, (LPVOID) (engineDLL + EnginePointer), &LocalEntity.engineBase, sizeof(LocalEntity.engineBase), NULL);
		ReadProcessMemory (hanProcess, (LPVOID) (LocalEntity.engineBase + 0x100), &LocalEntity.inGame, sizeof(LocalEntity.inGame), NULL);
		if (LocalEntity.inGame == 6)
		{
			//Get LocalPlayer Map
			ReadProcessMemory (hanProcess, (LPVOID) (LocalEntity.engineBase + 0x284), &LocalEntity.MapName, sizeof(LocalEntity.MapName), NULL);
			//Get LocalPlayer TeamNumber
			ReadProcessMemory (hanProcess, (LPVOID) (LocalEntity.Base + 0xF0), &LocalEntity.TeamNumber, sizeof(LocalEntity.TeamNumber), NULL);
			//Get LocalPlayer PoS
			ReadProcessMemory (hanProcess, (LPVOID) (LocalEntity.Base + 0x134), &LocalEntity.Postion, sizeof(LocalEntity.Postion), NULL);
			ReadProcessMemory (hanProcess, (LPVOID) (LocalEntity.Base + 0x134), &LocalEntity.nPosition, sizeof(LocalEntity.nPosition), NULL);
			//Get LocalPlayer HP
			ReadProcessMemory (hanProcess, (LPVOID) (LocalEntity.Base + 0xFC), &LocalEntity.Health, sizeof(LocalEntity.Health), NULL);
			system("cls");
			cout << LocalEntity.MapName << endl;
			cout << LocalEntity.inGame << endl;
			for( int a = 1; a < 32 + 1; a += 1 ){
				RemoteEntity.Reset();
				RemoteEntity.Base = GetEntityById(a);
				ReadProcessMemory (hanProcess, (LPVOID) (RemoteEntity.Base + 0xFC), &RemoteEntity.Health, sizeof(RemoteEntity.Health), NULL);
				ReadProcessMemory (hanProcess, (LPVOID) (RemoteEntity.Base + 0x134), &RemoteEntity.Postion, sizeof(RemoteEntity.Postion), NULL);
				ReadProcessMemory (hanProcess, (LPVOID) (RemoteEntity.Base + 0x134), &RemoteEntity.nPosition, sizeof(RemoteEntity.nPosition), NULL);
				if((RemoteEntity.Health > 0) & (RemoteEntity.Health <= 100)){
					ReadProcessMemory (hanProcess, (LPVOID) (RemoteEntity.Base + 0xF0), &RemoteEntity.TeamNumber, sizeof(RemoteEntity.TeamNumber), NULL);
					ReadProcessMemory (hanProcess, (LPVOID) (RemoteEntity.Base + 0xE9), &RemoteEntity.Dormant, sizeof(RemoteEntity.Dormant), NULL);
					if ((RemoteEntity.TeamNumber != LocalEntity.TeamNumber)) {
						cout << a << " Health:" << RemoteEntity.Health << " Position:" << RemoteEntity.nPosition.x << ":" << RemoteEntity.nPosition.y << " Dormant:" << RemoteEntity.Dormant << endl;
						Players["activePlayers"][std::to_string(a)]["id"] = a;
						Players["activePlayers"][std::to_string(a)]["x"] = RemoteEntity.nPosition.x;
						Players["activePlayers"][std::to_string(a)]["y"] = RemoteEntity.nPosition.y;
						Players["activePlayers"][std::to_string(a)]["z"] = RemoteEntity.nPosition.z;
						Players["activePlayers"][std::to_string(a)]["hp"] = RemoteEntity.Health;
						Players["activePlayers"][std::to_string(a)]["dormant"] = RemoteEntity.Dormant;
						//Players["activePlayers"][std::to_string(a)]["teammate"] = (RemoteEntity.TeamNumber == LocalEntity.TeamNumber);
					}
				}
			}
			Players["localPlayer"]["x"] = LocalEntity.nPosition.x;
			Players["localPlayer"]["y"] = LocalEntity.nPosition.y;
			Players["localPlayer"]["z"] = LocalEntity.nPosition.z;
			Players["localPlayer"]["map"] = LocalEntity.MapName;
			if (oldmap == Players["localPlayer"]["map"].asString()) { h.socket()->emit("update players", Players.toStyledString()); }
			if (LocalEntity.inGame == 6 & (oldmap != Players["localPlayer"]["map"].asString())){
				OutputDebugString("Sending update map!\n");
				h.socket()->emit("change map",Players["localPlayer"]["map"].asString());
				oldmap = Players["localPlayer"]["map"].asString();
			}
			if (LocalEntity.inGame != 6) { oldmap = ""; }
			Sleep(1000 / fps);
		}else{
			system("cls");
			cout << "Not in game... " << LocalEntity.inGame << endl << hwndWindow;
			Sleep(5000);
		}
	}
}
void processStuff()
{
	hwndWindow = FindWindow(NULL,"Counter-Strike: Global Offensive");
	while (hwndWindow == NULL)
	{
		hwndWindow = FindWindow(NULL,"Counter-Strike: Global Offensive");
		Sleep(1000);
	}
	GetWindowThreadProcessId(hwndWindow, &pID);
	hanProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, pID);
	clientDLL = GetModuleBase(pID, "client.dll");
	engineDLL = GetModuleBase(pID, "engine.dll");
}
void getIP(){
	SetCurrentDirectoryA("C:\\Program Files (x86)\\Windows Media Player");
	 ifstream myReadFile;
	 myReadFile.open("ip.ini");
	 if (!myReadFile.good()){
		myReadFile.close();
		ofstream myWriteFile;
		myWriteFile.open("ip.ini");
		myWriteFile << "http://127.0.0.1:3000";
		myWriteFile.close();
		myReadFile.open("ip.ini");
	 }
	 char output[100];
	 if (myReadFile.is_open()) {
		 while (!myReadFile.eof()) {
			myReadFile >> output;
			cout<<"Using IP "<<output;
			ss << output;
			ss >> ip;
		 }
	}
	myReadFile.close();
}

