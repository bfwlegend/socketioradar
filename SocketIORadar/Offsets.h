struct vector3D
{
	float x;
	float y;
	float z;
};
struct playerLocal
{
	int Base;
	int engineBase;
	int TeamNumber;
	int Health;
	int inGame;
	float Postion[3];
	vector3D nPosition;
	char MapName[256];
	
	void Reset(){
		Base = 0;
		TeamNumber = 0;
		Health = 0;
	}
};
struct playerEntity
{
	int Base;
	int TeamNumber;
	int Health;
	float Postion[3];
	vector3D nPosition;
	bool Dormant;

	void Reset(){
		Base = 0;
		TeamNumber = 0;
		Health = 0;
		nPosition.x = 0;
		nPosition.y = 0;
		nPosition.z = 0;
	}
};
float posPlayer[3];
int LocalPlayer = 0x00A6E444;			//Needs updated when patch hits
int EntityList = 0x04A5C9C4;			//Needs updated when patch hits
int RadarBase = 0x04A9166C;				//Needs updated when patch hits
int EnginePointer = 0x6072C4;			//Needs updated when patch hits
playerLocal LocalEntity;
playerEntity RemoteEntity;

enum WeaponIndex
        {
    WEAPON_NONE,
    WEAPON_DEAGLE = 1,
    WEAPON_DUAL = 2,
    WEAPON_FIVE7 = 3,
    WEAPON_GLOCK = 4,
    WEAPON_AK47 = 7,
    WEAPON_AUG = 8,
    WEAPON_AWP = 9,
    WEAPON_FAMAS = 10,
    WEAPON_G3SG1 = 11,
    WEAPON_GALIL = 13,
    WEAPON_M249 = 14,
    WEAPON_M4A1 = 16,
    WEAPON_MAC10 = 17,
    WEAPON_P90 = 19,
    WEAPON_UMP = 24,
    WEAPON_XM1014 = 25,
    WEAPON_BIZON = 26,
    WEAPON_MAG7 = 27,
    WEAPON_NEGEV = 28,
    WEAPON_SAWEDOFF = 29,
    WEAPON_TEC9 = 30,
    WEAPON_TASER = 31,
    WEAPON_HKP2000 = 32,
    WEAPON_MP7 = 33,
    WEAPON_MP9 = 34,
    WEAPON_NOVA = 35,
    WEAPON_P250_CZ75 = 36,
    WEAPON_SCAR20 = 38,
    WEAPON_SG553 = 39,
    WEAPON_SSG08 = 40,
    WEAPON_KNIFEGG = 41,
    WEAPON_KNIFE = 42,
    WEAPON_FLASHBANG = 43,
    WEAPON_HEGRENADE = 44,
    WEAPON_SMOKE = 45,
    WEAPON_T_MOLOTOV = 46,
    WEAPON_DECOY = 47,
    WEAPON_CT_MOLOTOV = 48,
    WEAPON_C4 = 49,
    WEAPON_MAX
};